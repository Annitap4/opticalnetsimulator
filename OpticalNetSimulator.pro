INCLUDEPATH += \
    /home/annita/Dropbox/aleph-w/aleph-1.2-b

LIBS += \
    -L/home/annita/Dropbox/aleph-w/aleph-1.2-b \
    -lAleph \
    -lnana \
    -lgsl \
    -lgslcblas \
    -lgmp \
    -lmpfr \
    -lasprintf

QMAKE_CXXFLAGS += -std=c++0x

HEADERS += \
    breadth_first_routing.H \
    configuration.H \
    configuration_panel.H \
    grid.H \
    grid_panel.H \
    grid_panel_listener.H \
    grid_window.H \
    misc.H \
    package.H \
    simulator.H \
    statistics.H \
    statistics_panel.H \
    statistics_per_element.H \
    statistics_shower_panel.H \
    sim_listener.H \
    deflection_routing.H \
    minimum_deflection_routing.H \
    scale_routing.H

SOURCES += \
    configuration.C \
    configuration_panel.C \
    grid.C \
    grid_panel.C \
    grid_window.C \
    main.C \
    simulator.C \
    statistics.C \
    statistics_panel.C \
    statistics_per_element.C \
    statistics_shower_panel.C \
    misc.C \
    package.C

FORMS += \
    configuration_form.ui \
    grid_form.ui \
    statistics_form.ui \
    statistics_shower_form.ui

CONFIG += warn_off

